********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: CRM API

Current Maintainer : 
Original Author    : Trellon LLC

General Links:
Project Page       : 
Handbook           : 
Support Queue      : 

Developer Links:

********************************************************************
DESCRIPTION:

This module aims at providing a API for standardizing CRM communications 
for Drupal. This will allow a foothold for other module developers to
develop modules for CRM systems <word> of the particular system being used.

It is currently in its earliest states of development and not ready for
deployment.

Michael Haggerty(Trellon) - Guidance, management, and documenting.

James Gilliland(Trellon) - Helped with initial development, admin interface and guidance.

Jonathan Pugh(Trellon) - Superman coder with a heart of gold.


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running. If you are
having problems, please review the Handbook, especially
http://drupal.org/node/43767.

Preparing for Installation:
---------------------------
Note: Please back up your site and database.

1. Place the module into your Drupal /sites/all/modules/ or particular site
   directory.

2. Enable your choice of modules by going to:
   > Administer > Site Building > Modules.

   In Drupal 5 CRM API modules are grouped in a CRM category.

3. A summary of modules and settings are available at your sites
   Administer -> Settings -> CRM API


********************************************************************
