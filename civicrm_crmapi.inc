<?php

/**
 * Enter description here...
 *
 * @param unknown_type $op
 * @param unknown_type $arg
 * @return unknown
 */
function civicrm_crmapi($op, $arg = NULL) {
  switch ($op) {
    case 'meta':
      return array('civicrm' => array(
        'name' => 'CiviCRM',
        'description' => 'CiviCRM is the first open source and freely downloadable constituent relationship management solution. CiviCRM is web-based, open source, internationalized, and designed specifically to meet the needs of advocacy, non-profit and non-governmental groups.',
        'version' => 0.1,
        'module' => 'civicrm',
      ));

    default:
      break;
  }
}


function civicrm_crmapi_contact_fields(){

  civicrm_initialize(TRUE);

  //Get default CiviCRM Profile
	$profile_id = variable_get("crmapi_civicrm_contact_profile", 1);

  //Get the fields for that profile
  $crm_fields = civicrm_uf_profile_fields_get($profile_id, true);

  $crmapi_contact_fields = crmapi_contact_fields();

  //dpr($crm_fields);

  //loop through crmapi standard crmapi fields, giving crm fields
	foreach ($crmapi_contact_fields as $crmapi_field => $crmapi_field_title) {

  	$crm_field = $crm_fields[civicrm_crmapi_contact_field_map($crmapi_field)];



		$fields[$crmapi_field] = array(
      '#crm_field_name' => $crm_field['name'],
      '#required' => $crm_field['is_required'],
      '#title' => $crmapi_field_title,
      '#type' => crmapi_contact_field_types($crmapi_field),
    );
  }

  return $fields;

}

function civicrm_crmapi_contact_field_map($crmapi_field){
    $field_map = array(


    );
}



/**
 * Implementation of hook_crmapi_contact_form
 *
 * @param int $uid
 * @return mixed additions to $user->crmapi['crmapi_user']
 */
function civicrm_crmapi_contact_form($edit=array(), $section=NULL){

  civicrm_initialize(TRUE);
  global $user;

  $form  = civicrm_crmapi_contact_fields();

  if($section=="user") {
    $fields = $edit['crmapi']['contact'];
  } else {
    $fields = $user['crmapi']['contact'];
  }
  foreach ($fields as $name => $value) {
      $form[$name]['#default_value'] = $value;
    }
  $visible_fields   = variable_get('crmapi_crmapi_'.$section.'_fields', array());
  $required_fields  = variable_get('crmapi_crmapi_'.$section.'_fields_requred', array());

  foreach($form as $index => $form_item) {
    if (!in_array($index, $visible_fields)) {
      unset($form[$index]);
    }
    if (in_array($index, $required_fields)) {
     $form[$index]['#required']=="true";
    }
  }

  $form['first_name']['#weight'] = 0;
  $form['last_name']['#weight'] = 1;
  $form['address1']['#weight'] = 2;
  $form['city']['#weight'] = 4;
  $form['state']['#weight'] = 5;
  $form['country']['#weight'] = 6;
  $form['postal_code']['#weight'] = 7;


  if ($category !='crmapi-contact') {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Submit',
      '#weight' => 10,
    );
  }

  return $form;
}

/**
 * Implementation of hook_crmapi_contact_form
 *
 * @param int $uid
 * @return mixed additions to $user->crmapi['crmapi_user']
 */
function civicrm_crmapi_contact_form_submit($form, &$form_state){

}
/**
 * Implementation of hook_crmapi_user
 *
 * @param int $uid
 * @return mixed additions to $user->crmapi['crmapi_user']
 */
function civicrm_crmapi_user($op, &$edit, &$account, $category = NULL){
  civicrm_initialize(TRUE);

  switch($op){
    case "view":
      return civicrm_crmapi_user_view($account);
      break;
    case 'update':
      return civicrm_form_data($edit, $account, $category, false, false );
  }

}



/**
* Implementation of hook_crmapi_user_contact_load
* Thanks, ECiviCRM!
**/

function civicrm_crmapi_user_contact_load($uid){

  civicrm_initialize(TRUE);
  // load contact from CiviCRM
  if (is_a($civicrm_cid = crm_uf_get_match_id($uid), 'CRM_Core_error')) {
    drupal_set_message('User ID not found in CiviCRM.', 'error');
    return false;
  };

  $contact =  civicrm_crmapi_contact_load($civicrm_cid, $uid);

  return $contact;
}


/**
 * implementation of hook_crmapi_contact_load();
 *
 * uid not required... added for reference.
 **/
function civicrm_crmapi_contact_load($civicrm_cid, $uid=0){

  civicrm_initialize(TRUE);

   if (is_a($crm_contact = crm_get_contact(array('contact_id' => $civicrm_cid)), 'CRM_Core_error')) {
    drupal_set_message('Contact not found in CiviCRM.', 'error');
    return false;
  };

  $contact =  array(
    'uid' => $uid,
    'crmapi_contact_id' => $civicrm_cid,

    'first_name'     => $crm_contact->contact_type_object->first_name,
    'last_name'      => $crm_contact->contact_type_object->last_name,
    'middle_name'    => $crm_contact->contact_type_object->middle_name,
    'prefix'         => $crm_contact->contact_type_object->prefix_id,
    'suffix'         => $crm_contact->contact_type_object->suffix_id,
    'display_name'   => $crm_contact->display_name,
    'sort_name'      => $crm_contact->sort_name,
    'email'          => $crm_contact->contact_type_object->location[1]->email[1]->email,

    'address1'       => $crm_contact->location[1]->address->street_address,
    //'address2'     => '',
    'city'           => $crm_contact->location[1]->address->city,
    'state'          => civicrm_transform_state($crm_contact->location[1]->address->state_province_id),
    'country'        => civicrm_transform_country($crm_contact->location[1]->address->country_id),
    'postal_code'    => $crm_contact->location[1]->address->postal_code,

    'crmapi_contact_data' => $crm_contact,
   /*
    'phone_home'     => ,
    'phone_work'     => 'Phone (Work)',
    'phone_work_ext' => 'Phone (Work Extension)',

    'organization'   => 'Organization',
    'occupation'     => 'Occupation',
    'employer'       => 'Employer',
   */
     );

/*
TODO: figure out how to handle multiple addresses per contact.
    foreach($crm_contact->location as $civicrm_location){
  //dpr($civicrm_location);
      $contact['addresses'][] = array(
        'address1'       => $civicrm_location->address->street_address,
        //'address2'     => '',
        'city'           => $civicrm_location->address->city,
        'state'          => civicrm_transform_state($civicrm_location->address->state_province_id),
        'country'        => civicrm_transform_country($civicrm_location->address->country_id),
        'postal_code'    => $civicrm_location->address->postal_code,
        );
    }
*/


    return $contact;

}



function civicrm_crmapi_user_view($user){

  $contact = $user->crmapi['contact'];

  //$output = theme("crmapi_contact", $contact);

  $output = implode(  "<br>",$user->crmapi['contact']);

  $items[t('CiviCRM')]['civicrm'] = array(
      'title' => t('Contact Information'),
      'value' => $output,
      'class' => 'crmapi_contact',
    );

	return $items;

}


/**
 * Issues a db_query() against the CiviCRM database.
 */
function civicrm_db_query($query) {
  global $db_url;                    // normally Drupal's DSN.
  $original_url = $db_url;           // we'll save it off temporarily.
  $db_url = CIVICRM_DSN;             // set it to the CiviCRM DSN.
  db_set_active('civicrm');          // then make CiviCRM active.
  $args = func_get_args();           // load all the args passed.
  array_shift($args);                // shove off the SQL query.
  $result = db_query($query, $args); // make the query against CiviCRM.
  $db_url = $original_url;           // toggle back to Drupal's DSN.
  db_set_active('default');          // and make that active again.
  return $result;                    // return CiviCRM database result.
}


/**
 * Given a CiviCRM country ID, return the name.
 */
function civicrm_transform_country($id = NULL) {
  $results = civicrm_db_query('SELECT cc.id, cc.name FROM {civicrm_address} ca LEFT JOIN {civicrm_country} cc ON (ca.country_id = cc.id) GROUP BY cc.name;');
  while ($result = db_fetch_object($results)) {
    if ($result->id == $id) { return $result->name; }
  }
}


/**
 * Given a CiviCRM country name, return the ID.
 */
function civicrm_transform_country_name($name = NULL) {
  $results = civicrm_db_query('SELECT cc.id, cc.name FROM {civicrm_address} ca LEFT JOIN {civicrm_country} cc ON (ca.country_id = cc.id) GROUP BY cc.name;');
  while ($result = db_fetch_object($results)) {
    if ($result->name == $name) { return $result->id; }
  }
}

/**
 * Given a CiviCRM state ID, return the two letter abbreviation.
 */
function civicrm_transform_state($id = NULL) {

  $states = civicrm_states_array();
  $states = array_flip($states); return $states[$id];
}


function civicrm_states_array($state_abbr = NULL) {
$states = array(
    'AL' => 1000,       'AK' => 1001,    'AZ' => 1002,       'AR' => 1003,
    'CA' => 1004,       'CO' => 1005,    'CT' => 1006,       'DC' => 1050,
    'DE' => 1007,       'FL' => 1008,    'GA' => 1009,       'HI' => 1010,
    'ID' => 1011,       'IL' => 1012,    'IN' => 1013,       'IA' => 1014,
    'KS' => 1015,       'KY' => 1016,    'LA' => 1017,       'ME' => 1018,
    'MD' => 1019,       'MA' => 1020,    'MI' => 1021,       'MN' => 1022,
    'MS' => 1023,       'MO' => 1024,    'MT' => 1025,       'NE' => 1026,
    'NV' => 1027,       'NH' => 1028,    'NJ' => 1029,       'NM' => 1030,
    'NY' => 1031,       'NC' => 1032,    'ND' => 1033,       'OH' => 1034,
    'OK' => 1035,       'OR' => 1036,    'PA' => 1037,       'RI' => 1038,
    'SC' => 1039,       'SD' => 1040,    'TN' => 1041,       'TX' => 1042,
    'UT' => 1043,       'VT' => 1044,    'VA' => 1045,       'WA' => 1046,
    'WV' => 1047,       'WI' => 1048,    'WY' => 1049,
  );
  if ($state_abbr) {
    return $states[$state_abbr];
  } else {
    return $states;
  }
}